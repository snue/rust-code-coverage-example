fn magic_value() -> u32 {
    17
}

fn main() {
    println!("Hello, world: {}", magic_value());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn magic_value_is_correct() {
        // This is of course not a good test. :)
        assert_eq!(magic_value(), 17);
    }
}
